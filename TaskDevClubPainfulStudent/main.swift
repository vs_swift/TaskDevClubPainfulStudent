//
//  main.swift
//  TaskDevClubPainfulStudent
//
//  Created by Private on 12/17/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

let John = Student(firstName: "John", lastName: "Johnson", profession: "artist")
let Veronica = Student(firstName: "Veronica", lastName: "Odell", profession: "singer")
let Jeff = Student(firstName: "Jeff", lastName: "Emerson", profession: "mover")
let Zach = Student(firstName: "Zach", lastName: "Hughes", profession: "cleaner")
let Kelly = Student(firstName: "Kelly", lastName: "Tye", profession: "dancer")
let George = Student(firstName: "George", lastName: "Dunnett", profession: "cashier")
let Roman = Student(firstName: "Roman", lastName: "Hayes", profession: "developer")
let Stacy = Student(firstName: "Stacy", lastName: "Stark", profession: "nurse")
let Julia = Student(firstName: "Julia", lastName: "Adams", profession: "teacher")
let Russell = Student(firstName: "Russell", lastName: "Davis", profession: "driver")

let team: [Int: Student] = [1: John, 2: Veronica, 3: Jeff, 4: Zach, 5: Kelly, 6: George, 7: Roman, 8: Stacy, 9: Julia, 10: Russell]

let group1: [Int: Student] = [1: John, 2: Veronica, 3: Jeff]
let group2: [Int: Student] = [1: Zach, 2: Kelly, 6: George]
let group3: [Int: Student] = [1: Roman, 2: Stacy, 9: Julia, 10: Russell]

for (key,value) in team {
    print("\(key):\(value.first_name,value.last_name, value.profession)")
}

for (key,value) in team.sorted(by: { $0.key < $1.key }) {
    print("\(key):\(value.profession)")
}

var teamArray = [[Int: Student]]()
teamArray.append(group1)
teamArray.append(group2)
teamArray.append(group3)

for i in stride(from:0, to: 3, by: 1) {
    for j in stride(from: 1, to: 3, by: 1) {
        if let lookUpDic = teamArray[i][j]{
            print(lookUpDic.display())
        }
    }
}




