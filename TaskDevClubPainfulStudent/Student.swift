//
//  Student.swift
//  TaskDevClubPainfulStudent
//
//  Created by Private on 12/17/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Student {
    let first_name: String
    let last_name: String
    let profession: String
    
    init(firstName: String, lastName: String, profession: String) {
        self.first_name = firstName
        self.last_name = lastName
        self.profession = profession
    }
    
    func display(){
        print("Fist Name: \(first_name)\nLast Name: \(last_name)\nProfession: \(profession)")
        return
    }
}
